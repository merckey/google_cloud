#!/bin/bash
set -e
# Maintained by lji226@uky.edu
echo Your container args are: "$@"
echo "============================"
echo Google Bucket = $GB
echo $INPUT
FILENAME=${INPUT##*/}
O_DIR=/tmp/result/"${FILENAME%.*}"
echo Output directory $O_DIR
mkdir -p $O_DIR
cd $O_DIR

python /tmp/dev/evolve.py \
$GB/$INPUT \
trees \
'top_k_trees' 'clonal_frequencies' 'llh_trace' 'rate_est' $NS $NMH $SEED $NORM 


cp -r /tmp/result/* $OUTPUT
