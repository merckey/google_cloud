sudo yum update
sudo yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel expat-devel wget
sudo yum -y group install "Development Tools"
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
sudo pip install dsub