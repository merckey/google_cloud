## Menghan's project

[READ dsub manual](https://github.com/googlegenomics/dsub)

#### Build docker.

  1. Make sure you are under your home directory. Clone this repo.

    ```
    cd ~/ && git clone https://merckey@bitbucket.org/merckey/google_cloud.git
    ```

  1. Upload your entire code directory `menghan_code` and put them under your home directory. The direcotry must be named as `menghan_code`. **NOTE**: the `example.R` file has been modified for docker to load several *.r files and *.so file required to run. All the dependencies are located under `/` directory in the docker container. So make sure `source` function and `dyn.load` have the source file path like this, `source("/function_library.r")` instead of `source("function_library.r")`.

    ```
    ls menghan_code
    ```

  1. Build and tag. This will only work when your current path is your home directory

    ```
    sudo docker build . -f google_cloud/menghan/Dockerfile_simp -t menghan:latest  
    ```
    
    ```
    sudo docker tag menghan:latest us.gcr.io/nih-commons-credit-project/menghan:latest
    ```

  1. Push to container registry

    ```
    sudo gcloud docker -- push us.gcr.io/nih-commons-credit-project/menghan:latest
    ```

#### Debug on local instance with dsub
Before you do this make sure you have the following 2 files in your google cloud bucket, in my case `gs://bucket1q2w/jpliu/menghan/data/`.

  1. `gs:bucket1q2w/jpliu/menghan/data/TCGA.BLCA.mutect.8ba9ebf3-adc9-44b2-a449-36804386b518.somatic.maf`

The command below will not submit job by creating new instance, but run on your current working instance.

After the run, you will find the `12mle.txt` and `12pairprob.txt` under your google bucket `gs://bucket1q2w/jpliu/menghan/results/`


```
dsub \
  --provider local \
  --logging gs://bucket1q2w/jpliu/logging/ \
  --image us.gcr.io/nih-commons-credit-project/menghan:latest \
  --input INPUT="gs://bucket1q2w/jpliu/menghan/data/TCGA.BLCA.mutect.8ba9ebf3-adc9-44b2-a449-36804386b518.somatic.maf" \
  --output OUTPUT="gs://bucket1q2w/jpliu/menghan/results/*" \
  --env arg1=1 \
  --env arg2=2 \
  --command 'Rscript /example.R $arg1 $arg2 $(dirname $OUTPUT) $INPUT' \
  --wait
```


#### Run by creating new instance for single job
To submit job on new instance, provide your project id and zone.

```
dsub \
  --project nih-commons-credit-project \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/logging/ \
  --image us.gcr.io/nih-commons-credit-project/menghan:latest \
  --input INPUT="gs://bucket1q2w/jpliu/menghan/data/TCGA.BLCA.mutect.8ba9ebf3-adc9-44b2-a449-36804386b518.somatic.maf" \
  --output OUTPUT="gs://bucket1q2w/jpliu/menghan/results/*" \
  --env arg1=1 \
  --env arg2=2 \
  --command 'Rscript /example.R $arg1 $arg2 $(dirname $OUTPUT) $INPUT' \
  --wait
```
#### Specify number of cores, RAMS, disk-size
`dsub` allows you to change the instance computing environments your program required. For example, if you need minimum cpu cores = 4, memory = 10G,
disk-size = 50G, you can add addition paramters to the dsub command.
```
dsub \
  --min-cores 4 \
  --min-ram 10 \
  --disk-size 50 \
  --project nih-commons-credit-project \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/logging/ \
  --image us.gcr.io/nih-commons-credit-project/menghan:latest \
  --input INPUT="gs://bucket1q2w/jpliu/menghan/data/TCGA.BLCA.mutect.8ba9ebf3-adc9-44b2-a449-36804386b518.somatic.maf" \
  --output OUTPUT="gs://bucket1q2w/jpliu/menghan/results/*" \
  --env arg1=1 \
  --env arg2=2 \
  --command 'Rscript /example.R $arg1 $arg2 $(dirname $OUTPUT) $INPUT' \
  --wait
```

#### Run by creating new instances for batch jobs
Store all the parameters in the `example.tsv` file under folder `google_cloud/menghan/`.
```
dsub \
  --project nih-commons-credit-project \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/logging/ \
  --image us.gcr.io/nih-commons-credit-project/menghan:latest \
  --command 'Rscript /example.R $arg1 $arg2 $(dirname $OUTPUT) $INPUT' \
  --tasks google_cloud/menghan/example.tsv \
  --wait
```

------

# Deprecation

## Build docker image

```
git clone https://merckey@bitbucket.org/merckey/google_cloud.git
docker build google_cloud/menghan -f Dockerfile_simp --build-arg DUMMY=`date +%s` -t menghan/test

```

## RUN on local instance 
```
docker run -it -v ~/data/:/data menghan/test bash run_example.sh 1 2 /data /data/TCGA.BLCA.mutect.8ba9ebf3-adc9-44b2-a449-36804386b518.somatic.maf
```

## RUN with dsub

#### Push docker images to the Container Registry
You will need to change the project-id `nih-commons-credit-project` to your
google project id.

```
docker tag menghan/test us.gcr.io/nih-commons-credit-project/menghan:test
sudo gcloud docker -- push us.gcr.io/nih-commons-credit-project/menghan:test
```

#### Upload your data to your google bucket folder
In this example, I saved the test input data under the bucket `gs://bucket1q2w/jpliu/menghan/data/`

#### Run dsub on 1 sample

```
dsub \
--project nih-commons-credit-project \
--zones "us-central1-*" \
--logging gs://bucket1q2w/jpliu/logging/ \
--output OUT=gs://bucket1q2w/jpliu/menghan/results/pairprob.txt \
--env DIR=gs://bucket1q2w/jpliu/menghan/results/ \
--input IN=gs://bucket1q2w/jpliu/menghan/data/TCGA.BLCA.mutect.8ba9ebf3-adc9-44b2-a449-36804386b518.somatic.maf \
--image us.gcr.io/nih-commons-credit-project/menghan:test \
--command 'bash run_example.sh 1 2 ${DIR} ${IN}' \
--wait
```
