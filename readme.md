# Google cloud 

This repo aims to help build the google cloud computing instance using Centos 7 as your base OS. You can use this instance for testing and batch job submission.
**The following steps only need to run for creating an new instance**. After successfully setup, you do not need to re-run the whole steps again. The only
command you will need to run once you turn it back on, is `sudo systemctl start docker`. This will restart the docker daemon for any docker related jobs.

Rules:
  * Do not let your instance idle for a long time. `STOP` it when you do not use it. This will maximumly reduce the waste of resources.


Please follow the steps below to setup your development environemnt. You can then continue your project guide.

  * **Tingting:** [guide](tingting_project.md)
  * **Austin:** [guide](austin_project.md)
  * **Menghan:** [guide](menghan_project.md)
  

# Enable Google API
 ** Manually Enable **
 
## 1. Initialize centos 7 base
NOTES:
  
  * Base operating system must be Centos 7.
  * When manually creating the base OS, make sure **Cloud API access scopes** has the check mark on 
  **Allow full access to all Cloud APIs**. Otherwise, some APIs will not have permission to do things
  like creating a bucket for container registry.
  
```
sudo yum update
sudo yum install -y zlib-devel bzip2-devel openssl-devel ncurses-devel sqlite-devel readline-devel tk-devel gdbm-devel db4-devel libpcap-devel xz-devel expat-devel git
sudo yum -y group install "Development Tools"
sudo yum -y install wget gsl-devel 
wget https://bootstrap.pypa.io/get-pip.py
sudo python get-pip.py
rm get-pip.py
sudo pip install dsub
```

## 2. Install Docker
[Ref1](https://cloud.google.com/compute/docs/containers/#installing)
[Centos](https://docs.docker.com/install/linux/docker-ce/centos/)
```
sudo yum install -y yum-utils \
  device-mapper-persistent-data \
  lvm2
  
sudo yum-config-manager \
    --add-repo \
    https://download.docker.com/linux/centos/docker-ce.repo

sudo yum install docker-ce

sudo usermod -a -G docker ${USER}
```

#### Start Docker
```
sudo systemctl start docker
```

Run hello world
```
sudo docker run hello-world
```


## 3. Google Cloud SDK
[about](https://cloud.google.com/sdk/downloads#yum#)

The SDK is default installation on the compute engine
```
gcloud init

# then put your project id in the concole
```

## 4. Manage Container registry
[about](https://cloud.google.com/container-registry/docs/managing)

#### list images
```
gcloud container images list --repository us.gcr.io/nih-commons-credit-project

NAME
us.gcr.io/nih-commons-credit-project/menghan
us.gcr.io/nih-commons-credit-project/phylowgs
```


## Common Errors

> Cannot connect to the Docker daemon at unix:///var/run/docker.sock. Is the docker daemon running?
This error is due to the docker server is down. Start it with the following cmd.
```
sudo systemctl start docker
```

```
s://crdc-weiss-heidi-2018-mtb-r01
```
