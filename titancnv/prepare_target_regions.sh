wget http://hgdownload.soe.ucsc.edu/goldenPath/hg19/liftOver/hg19ToHg38.over.chain.gz
# 5860504001, remove the tiled_region tracks and keep only 1-22XY
sed '/tiled_region/Q' ../data/SeqCap_EZ_Exome_v2.bed | grep -E "chr[0-9XY]+\s" > ../data/SeqCap_EZ_Exome_v2.hg19_filtered.bed
titancnv/liftOver ../data/SeqCap_EZ_Exome_v2.hg19_filtered.bed hg19ToHg38.over.chain ../data/SeqCap_EZ_Exome_v2.hg38_filtered.bed ../data/SeqCap_EZ_Exome_v2.hg38_filtered.fail
grep -E "chr[0-9XY]+\s" data/SeqCap_EZ_Exome_v2.hg38_filtered.bed > data/SeqCap_EZ_Exome_v2.hg38_clean.bed

# https://earray.chem.agilent.com/suredesign/home.htm
