## Access Controlled TCGA data
https://github.com/isb-cgc/examples-R/blob/master/inst/doc/Processing_Controlled_Data_With_Bioconductor.md
## Data

  * ftp://ftp.broadinstitute.org/pub/seq/references/Homo_sapiens_assembly19.fasta
  * https://github.com/broadinstitute/ichorCNA/blob/master/inst/extdata/GRCh37.p13_centromere_UCSC-gapTable.txt
  * ftp://gsapubftp-anonymous@ftp.broadinstitute.org/bundle/b37/hapmap_3.3.b37.vcf.gz

#### hg38

  * ftp://ftp.broadinstitute.org/bundle/hg38/
  * gsutil cp gs://genomics-public-data/resources/broad/hg38/v0/Homo_sapien
s_assembly38.fasta .

#### build the docker

docker build . -t titancna:build  --build-arg DUMMY=d

sudo docker tag titancna:build us.gcr.io/nih-commons-credit-project/titancna:latest

gcloud docker -- push us.gcr.io/nih-commons-credit-project/titancna:latest

## Debug in local docker

```
docker run -ti --entrypoint bash titancnv:build

docker run -ti --entrypoint bash -v ~/data/:/data/ titancna:build

docker run -ti --entrypoint bash -v ~/data/:/data/ -v ~/titancna/:/home/usr/titancna/ -v ~/results/:/home/usr/titancna/scripts/snakemake/results titancna:build

snakemake -s TitanCNA.snakefile --cores 5 -p
```

## Run on single on one instance
# todo: add success log

In dsub run:
```
dsub \
  --min-cores 4 \
  --min-ram 10 \
  --disk-size 50 \
  --project nih-commons-credit-project \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/titancna/tcga/coad/logging \
  --image us.gcr.io/nih-commons-credit-project/titancna:latest \
  --input-recursive liftovers=gs://bucket1q2w/jpliu/titancna/tcga/liftovers \
  --input-recursive refs=gs://bucket1q2w/jpliu/titancna/tcga/refgrch38/ \
  --input bamMeta=gs://bucket1q2w/jpliu/titancna/tcga/coad_pass_bam.tsv \
  --input samplesList=gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_filtered_samples.yaml \
  --input pairList=gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_filtered_pairings_kit1.yaml \
  --input token=gs://bucket1q2w/jpliu/titancna/tcga/token.txt \
  --output-recursive OUTPUT_DIR=gs://bucket1q2w/jpliu/titancna/tcga/coad \
  --tasks data/run1.tsv \
  --command "cd /home/usr/titancna/scripts/snakemake/ && bash runTitan.sh" \
  --wait
```


```
# debug recursive
dsub \
  --project nih-commons-credit-project \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/titancna/logging \
  --image us.gcr.io/nih-commons-credit-project/titancna:latest \
  --output-recursive OUTPUT_DIR=gs://bucket1q2w/jpliu/titancna/tcga/coad \
  --command 'cd /home/usr/titancna/scripts/snakemake/ && mkdir -p results/titan/hmm && touch results/titan/hmm/hha.txt && cp -r results/titan/hmm "${OUTPUT_DIR}"'\
  --wait
```

```
dsub \
  --provider local \
  --image titancna:build \
  --input-recursive INPUT_PATH=gs://bucket1q2w/jpliu/titancna/tcga/liftovers \
  --command 'ls /mnt/data/input/gs/bucket1q2w/jpliu/titancna/tcga/liftovers' \
  --log dsublog \
  --wait
```














gdc-client download -t /data/gdc-user-token.2018-08-11T03_48_11.033Z.txt -d /data/ -n 5 -m /data/coad_test.tsv

## Cp between instance
gcloud compute scp jp-dev:~/data/ ~/ --recurse --zone us-east1-b
[lji226@jp-dev titancnv]$ docker tag titancna:build us.gcr.io/kyrp-198117/titancna:build
[lji226@jp-dev titancnv]$ gcloud docker -- push us.gcr.io/kyrp-198117/titancna:build
gcloud docker -- pull us.gcr.io/kyrp-198117/titancna:build


## debug phylowgs

docker run -ti --entrypoint bash -v ~/phylowgs_test/:/phylowgs/ phylowgs:jp

python /tmp/phylowgs/evolve.py /phylowgs/ssm_data.txt /phylowgs/cnv_data.txt

gsutil cp gs://phylowgs/OUTPUT/ssm_data_coad_TCGA-4N-A93T-01A-11D-A36X-10/trees.zip .




python /tmp/phylowgs/parser/create_phylowgs_inputs.py --cnv sample1=cnvs.txt --vcf-type sample1=mutect_smchet sample1=snv_cnv_filtered/TCGA-4N-A93T.filtered.vcf --output-cnvs snv_cnv_filtered/cnv_data.txt --output-variants snv_cnv_filtered/ssm_data.txt


python /tmp/phylowgs/evolve.py ssm_data.txt cnv_data.txt



python /tmp/phylowgs/print_tree.py trees.zip top5.txt TCGA-4N-A93T.filtered.mapping.txt

find /data/ -name "*.bai" -exec grep -lv ".bam.bai" {} \; -exec bash -c 'mv $1 ${1%.bai}.bam.bai' - '{}' \;