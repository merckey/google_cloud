#!/bin/bash
set -e
# 
# Usage: bash submit_dsub.sh -p PREFIX -l FAIL -i INTERVAL
usage() {
  cat << EOF
  usage: $0 options
  Generate job lists and submit dsub jobs with delay
  OPTIONS:
  -h      Show help message
  -p      PREFIX
  -l      FAIL
  -i      INTERVAL, can be s(second), m(minute), or h(hour)
EOF
}

while getopts "hp:l:i:" OPTION
do
  case $OPTION in
    h)
      usage
      exit 1
    ;;
    p)
      PREFIX=$OPTARG
    ;;
    l)
      FAIL=$OPTARG
    ;;
    i)
	    INTERVAL=$OPTARG
    ;;
    \?)
      usage
      exit
    ;;
  esac
done


# I have processed the first 14 samples, so will skip them
echo Split files with prefix: ${PREFIX}
echo Clean old files!
rm -f ${PREFIX}*

cat $FAIL | split -l 20 -d \
--filter='{ printf %s\\n "run"; cat; } > $FILE' - ${PREFIX}

jobs=$(ls $PREFIX*)

for job in $jobs; do
  echo ... submit job ${job}!
  dsub \
    --min-cores 4 \
    --min-ram 10 \
    --disk-size 200 \
    --project nih-commons-credit-project \
    --zones "us-east1-*" \
    --logging gs://bucket1q2w/jpliu/titancna/tcga/coad/logging \
    --image us.gcr.io/nih-commons-credit-project/titancna:latest \
    --input-recursive liftovers=gs://bucket1q2w/jpliu/titancna/tcga/liftovers \
    --input-recursive refs=gs://bucket1q2w/jpliu/titancna/tcga/refgrch38/ \
    --input bamMeta=gs://bucket1q2w/jpliu/titancna/tcga/coad_pass_bam.tsv \
    --input samplesList=gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_filtered_samples.yaml \
    --input pairList=gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_filtered_pairings_kit1_2.yaml \
    --input token=gs://bucket1q2w/jpliu/titancna/tcga/token.txt \
    --output-recursive OUTPUT_DIR=gs://bucket1q2w/jpliu/titancna/tcga/coad \
    --tasks ${job} \
    --command "cd /home/usr/titancna/scripts/snakemake/ && bash runTitan.sh"
  echo sleep for ${INTERVAL} ...
  sleep ${INTERVAL}
done



