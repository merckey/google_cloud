BUCKET=gs://bucket1q2w/jpliu/titancna/tcga/coad/
tail -n +2  $1 \
| sed "s/\s//g" | sed "s/:.*//g" | sort > expected.txt


# list all the solution files
gsutil ls ${BUCKET}** | grep optimalClusterSolution.txt > bucket_done

rm -f done.txt
for s in $(cat bucket_done );do 
	echo $(basename ${s%/*}) >> done.txt
done

comm -13 <(sort done.txt) <(sort expected.txt) > failure.txt
# for sample in $SAMPLES; do
# 	echo $sample
# 	gsutil -q stat ${BUCKET}/${sample}/optimalClusterSolution.txt

# 	return_value=$?

# 	if [ $return_value = 0 ]; then
# 	    echo "folder exist"
# 	else
# 	    echo "folder does not exist"
# 	fi
# done