#set -e
echo Provide the job list tsv file!
JOBS=$1

JOB_STATUS=gs://bucket1q2w/phylowgs_mc3/pan12/titancna_jobs/

tail -n +2  $1 > expected.txt

rm -rf bucket_done failed.txt

for s in $(gsutil ls ${JOB_STATUS}*.titanca.done);do 
	echo $(basename ${s}) >> bucket_done
done

sed -i 's/\.titanca\.done//' bucket_done


comm -13 <(sort bucket_done) <(sort expected.txt) > failed.txt
sed  -i '1i run' failed.txt


echo "Check the failed.txt before you submit the job!"
echo "Proceed (y/n)? "
read answer

if [ "$answer" != "${answer#[Yy]}" ] ;then
    dsub \
	--min-cores 4 \
	--min-ram 10 \
	--disk-size 300 \
        --provider google-v2 \
	--project nih-commons-credit-project \
	--zones "us-east1-*" \
	--logging gs://bucket1q2w/phylowgs_mc3/titancna_logging/ \
	--image us.gcr.io/nih-commons-credit-project/titancna:mc3 \
	--input-recursive refs=gs://bucket1q2w/hg38_chr/ \
	--input bamMeta=gs://bucket1q2w/phylowgs_mc3/pan12/meta.pan12.paired_bam_hg38_chr.tsv \
	--input samplesList=gs://bucket1q2w/phylowgs_mc3/pan12/samples.pan12.mc3_hg38_chr.yaml \
	--input pairList=gs://bucket1q2w/phylowgs_mc3/pan12/pairs.pan12.mc3_hg38_chr.yaml \
	--output-recursive OUTPUT_DIR=gs://bucket1q2w/phylowgs_mc3/pan12/titancna_res/ \
	--output-recursive JOB_STATUS=gs://bucket1q2w/phylowgs_mc3/pan12/titancna_jobs/ \
	--tasks failed.txt \
	--command "cd /home/usr/titancna/scripts/snakemake/ && bash runTitan.sh" \
	--wait
else
    echo Exit without submit!
fi




