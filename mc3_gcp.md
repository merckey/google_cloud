## Submit jobs
```
# testing
bash google_cloud/titancnv/handle_mc3_dsub.sh test/run1.tsv

# run
# compare finished job with /home/chiwang3/hg38_chr/jobs.pan12.mc3_hg38_chr.tsv
# only submit unprocessed ones.
bash google_cloud/titancnv/handle_mc3_dsub.sh /home/chiwang3/hg38_chr/jobs.pan12.mc3_hg38_chr.tsv
```


## reference data

```
gsutil ls gs://bucket1q2w/hg38_chr/
gs://bucket1q2w/hg38_chr/GRCh38.GCA_000001405.2_centromere_acen.txt
gs://bucket1q2w/hg38_chr/Homo_sapiens_assembly38.fasta
gs://bucket1q2w/hg38_chr/Homo_sapiens_assembly38.fasta.fai
gs://bucket1q2w/hg38_chr/cytoBand_hg38.txt
gs://bucket1q2w/hg38_chr/gc_hg38_10kb.wig
gs://bucket1q2w/hg38_chr/gencode.v19_to_hg38_chr_filtered.bed
gs://bucket1q2w/hg38_chr/hapmap_3.3.hg38.vcf
gs://bucket1q2w/hg38_chr/map_hg38_10kb.wig
```

## pan12 meta
```
gsutil ls gs://bucket1q2w/phylowgs_mc3/pan12/
gs://bucket1q2w/phylowgs_mc3/pan12/
gs://bucket1q2w/phylowgs_mc3/pan12/gencode.v19_to_hg38_chr_filtered.bed
gs://bucket1q2w/phylowgs_mc3/pan12/jobs.pan12.mc3_hg38_chr.tsv
gs://bucket1q2w/phylowgs_mc3/pan12/meta.pan12.paired_bam_hg38_chr.tsv
gs://bucket1q2w/phylowgs_mc3/pan12/pairs.pan12.mc3_hg38_chr.yaml
gs://bucket1q2w/phylowgs_mc3/pan12/samples.pan12.mc3_hg38_chr.yaml
gs://bucket1q2w/phylowgs_mc3/pan12/titancna_jobs/
```

  * `gs://bucket1q2w/phylowgs_mc3/pan12/titancna_res`: The results folder for all titancna runs. 
  * `gs://bucket1q2w/phylowgs_mc3/pan12/titancna_jobs/`: Samples with successful titancan run. 

