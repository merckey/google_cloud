## phyloWGS
[READ dsub manual](https://github.com/googlegenomics/dsub)

#### Build phyloWGS docker.

  1. Make sure you are under your home directory. Clone this repo.

    ```
    cd ~/ && git clone https://merckey@bitbucket.org/merckey/google_cloud.git
    ```

  1. Upload your entire code directory `phylowgs` and put them under your home directory

    ```
    ls phylowgs
    ```

  1. Build and tag. This will only work when your current path is your home directory

    ```
    sudo docker build . -f google_cloud/phylowgs/Dockerfile -t phylowgs:latest  
    ```
    
    ```
    sudo docker tag phylowgs:latest us.gcr.io/nih-commons-credit-project/phylowgs:latest
    ```

  1. Push to container registry

    ```
    sudo gcloud docker -- push us.gcr.io/nih-commons-credit-project/phylowgs:latest
    ```

#### Debug on local instance with dsub
Before you do this make sure you have the following 2 files in your google cloud bucket, in my case `gs://bucket1q2w/jpliu/phylowgs/`.

  1. `gs://bucket1q2w/jpliu/phylowgs/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10.txt`
  1. `gs://bucket1q2w/jpliu/phylowgs/cnv_data.txt`

The command below will not submit job by creating new instance, but run on your current working instance.

After the run, you will find the `trees.zip` file under your google bucket `gs://bucket1q2w/jpliu/phylowgs/results/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10/`


```
dsub \
  --provider local \
  --logging gs://bucket1q2w/jpliu/logging/ \
  --image us.gcr.io/nih-commons-credit-project/phylowgs:latest \
  --input INPUT="gs://bucket1q2w/jpliu/phylowgs/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10.txt" \
  --input CNA="gs://bucket1q2w/jpliu/phylowgs/cnv_data.txt" \
  --output OUTPUT_DIR="gs://bucket1q2w/jpliu/phylowgs/results/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10/trees.zip" \
  --env B=10 \
  --env S=50 \
  --env I=100 \
  --command '/tmp/phylowgs/run_phylowgs.sh' \
  --wait
```

Variables you need to change for each run:

   * INPUT: the input file path to google bucket
   * OUTPUT_DIR: the google bucket path where you want to store your output file.
   * B: the `-B` option in evolv3.py
   * S: the `-s` option in evolv3.py
   * i: the `-i` option in evolve3.py

#### Run by creating new instance for single job
To submit job on new instance, provide your project id and zone.

```
dsub \
  --project nih-commons-credit-project \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/logging/ \
  --image us.gcr.io/nih-commons-credit-project/phylowgs:latest \
  --input INPUT="gs://bucket1q2w/jpliu/phylowgs/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10.txt" \
  --input CNA="gs://bucket1q2w/jpliu/phylowgs/cnv_data.txt" \
  --output OUTPUT_DIR="gs://bucket1q2w/jpliu/phylowgs/results/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10/trees.zip" \
  --env B=10 \
  --env S=50 \
  --env I=100 \
  --command '/tmp/phylowgs/run_phylowgs.sh' \
  --wait
```

#### Specify number of cores, RAMS, disk-size
`dsub` allows you to change the instance computing environments your program required. For example, if you need minimum cpu cores = 4, memory = 10G,
disk-size = 50G, you can add addition paramters to the dsub command.
```
dsub \
  --min-cores 4 \
  --min-ram 10 \
  --disk-size 50 \
  --project nih-commons-credit-project \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/logging/ \
  --image us.gcr.io/nih-commons-credit-project/phylowgs:latest \
  --input INPUT="gs://bucket1q2w/jpliu/phylowgs/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10.txt" \
  --input CNA="gs://bucket1q2w/jpliu/phylowgs/cnv_data.txt" \
  --output OUTPUT_DIR="gs://bucket1q2w/jpliu/phylowgs/results/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10/trees.zip" \
  --env B=10 \
  --env S=50 \
  --env I=100 \
  --command '/tmp/phylowgs/run_phylowgs.sh' \
  --wait
```

#### Run by creating new instances for batch jobs
Store all the parameters in the `example.tsv` file under folder `google_cloud/phylowgs/`.
```
dsub \
  --project nih-commons-credit-project \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/logging/ \
  --image us.gcr.io/nih-commons-credit-project/phylowgs:latest \
  --command '/tmp/phylowgs/run_phylowgs.sh' \
  --tasks google_cloud/phylowgs/example.tsv
  --wait
```

## TitanCNA

#### 1. prepare inputs on local
Create required input files before you run the TitanCNA program. This section only tested
on local laptop. Many of the scripts may need to be tuned based on different TCGA projects.

1. create a list samples for the projects. Should pass the required filters, e.g,
max mutation frequency for the specific tumor type?
1. create the paired tumor-normal bam file list based on the sample list. This step 
will further filter the samples based on:
    - the availability of the exome target files
    - the availability of the bam files
1. download the exome target files based on the target kit codes or names. The target
files are required for each bam files included for the TitanCNA runs. The bed file must be preprocess to liftover to hg38
using UCSC liftover tool kit. In the meantime, only keep Chr1-22,X and Y. Remove all other regions. The chromosomes must start with "Chr".
1. download the final vcf files for the final list of samples. For each sample, subset 
the vcf file based on the project's maf file. In the sametime, create the mapping 
between the vcf file and the corresponding annotation (GENEID_VariantType_ProteinChange).

Upload all the input files to a Google Bucket to allow cloud access

Scripts used to create the inputs
```
Need to fill in
```

#### 2. TCGA COAD example
Below is an example to submit dsub jobs for a batch of 15 samples using `run1.tsv`. They are the first 15 samples from `TCGA_COAD_filtered_pairings_kit1.yaml`.

Before the job submission, check the cloud bucket to make sure you have all the required files.

```
gsutil ls gs://bucket1q2w/jpliu/titancna/tcga/
gs://bucket1q2w/jpliu/titancna/tcga/
# define the tumor normal pairs
gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_filtered_pairings.yaml
# a subset of samples using exome kit1, ignore this file
gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_filtered_pairings_kit1.yaml
# all TCGA COAD samples
gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_filtered_samples.yaml
# a subset of pairs for testing
gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_test_pairings.yaml
# the TCGA COAD meta data file, mapping bam files to exome bait kits
gs://bucket1q2w/jpliu/titancna/tcga/coad_pass_bam.tsv
# the GDC token
gs://bucket1q2w/jpliu/titancna/tcga/token.txt
# folder to titancna results
gs://bucket1q2w/jpliu/titancna/tcga/coad/
# folder to exome regions, must be preprocess to convert to hg38
# in the meantime, only keep chr1-22,X and Y. Remove all other regions.
gs://bucket1q2w/jpliu/titancna/tcga/liftovers/
# folder for all the required references
gs://bucket1q2w/jpliu/titancna/tcga/refgrch38/
```

Use dsub to submit jobs. You only need to change this line for job submission `--tasks run1.tsv`. `run1.tsv` is the local files contains only the tumor ids. You can create a similar run1.tsv file using TCGA_COAD_filtered_pairings_kit1.yaml. The sample ids are the barcode on the left side of each colon. 

```
dsub \
  --min-cores 4 \
  --min-ram 10 \
  --disk-size 50 \
  --project nih-commons-credit-project \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/titancna/tcga/coad/logging \
  --image us.gcr.io/nih-commons-credit-project/titancna:latest \
  --input-recursive liftovers=gs://bucket1q2w/jpliu/titancna/tcga/liftovers \
  --input-recursive refs=gs://bucket1q2w/jpliu/titancna/tcga/refgrch38/ \
  --input bamMeta=gs://bucket1q2w/jpliu/titancna/tcga/coad_pass_bam.tsv \
  --input samplesList=gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_filtered_samples.yaml \
  --input pairList=gs://bucket1q2w/jpliu/titancna/tcga/TCGA_COAD_filtered_pairings_kit1.yaml \
  --input token=gs://bucket1q2w/jpliu/titancna/tcga/token.txt \
  --output-recursive OUTPUT_DIR=gs://bucket1q2w/jpliu/titancna/tcga/coad \
  --tasks data/run1.tsv \
  --command "cd /home/usr/titancna/scripts/snakemake/ && bash runTitan.sh" \
  --wait

```

The `google_cloud/titancnv/submit_dsub.sh` script will read the pairings yaml file and split the samples into 20 samples per pieces and then submit dsub jobs with user defined time intervals.

```
bash google_cloud/titancnv/submit_dsub.sh -p coad_kit1_ -l data/TCGA_COAD_filtered_pairings_kit1.yaml -i 15m
```
  * -p: job file prefix
  * -l: pairings yaml file
  * -i: time intervals, can be seconds, minutes or hours. This is controled by the suffix s, m or h.

Get failure runs. The script will check with the parilist to see whether each sample has titancna solution results on the cloud bucket.
If not found, it will save the failed job to failure.txt file. In the meantime, finished jobs will be saved in done.txt

```
bash google_cloud/titancnv/check_failure.sh
```
Output:
  * failure.txt  Jobs failed.
  * done.txt Jobs finished.
  * expected.txt The master job list.

To re-submit the failed job, use the script below to submit jobs in `failure.txt`
```
bash google_cloud/titancnv/submit_dsub_failure.sh -p coad_kit1_failure_ -l failure.txt -i 15m
```


#### 3. PhyloWGS with cnv and snv data from TCGA

The following script will process the cnv and snv data to generate the inputs for PhyloWGS.

```
bash processSolution.sh /home/lji226/done.txt /home/lji226/data/coad/vcf
``` 
Input:
  * The list of finished titancnv jobs
  * The vcf files for the samples in the `done.txt` list. The vcf was derived from the GDC vcf file. Varations not matching the maf file
  will be filtered out.

Output:
  * cnv_data.txt
  * ssm_data.txt


  

###### Debug:

The code below will start a titancna container. It mount the `~/data`, `~/titancna` and
`~/results` folders on the container for efficient debugging. The required reference genome and bam files
 are in `~/data/` directory. The titiancna output will be in `~/results`.


1. Start the container

3. Start a phylowgs container
```
docker run -ti --entrypoint bash -v ~/phylowgs_test/:/phylowgs/ phylowgs:jp

# creat input using the vcf and cnvs.txt from titancna
python /tmp/phylowgs/parser/create_phylowgs_inputs.py --cnv sample1=cnvs.txt --vcf-type sample1=mutect_smchet sample1=snv_cnv_filtered/TCGA-4N-A93T.filtered.vcf --output-cnvs snv_cnv_filtered/cnv_data.txt --output-variants snv_cnv_filtered/ssm_data.txt

# run the evolve 
python /tmp/phylowgs/evolve.py /snv_cnv_filtered/ssm_data.txt /snv_cnv_filtered/cnv_data.txt

# print tree
python /tmp/phylowgs/print_tree.py trees.zip top5.txt TCGA-4N-A93T.filtered.mapping.txt
```
------  

# Deprecation

#### RUN with Docker
```
docker run -e INPUT="/phylowgs/input/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10.txt" \
-e OUTPUT_DIR='/phylowgs/ssm_data_coad_TCGA-3L-AA1B-01A-11D-A36X-10/trees.zip' \
-v ~/phylowgs:/phylowgs -e I=100 -e B=10 -e S=50 -ti phylowgs:latest

```


Project bucket:

2000 - 6 h

200 - 20min

#### Installation on the base instance:
```
sudo pip install numpy scipy ete2
cd phylowgs
git clone https://merckey@bitbucket.org/merckey/phylowgs.git
g++ -o mh.o -O3 mh.cpp  util.cpp `gsl-config --cflags --libs`
```

#### Run:
```
python evolve3.py ssm_data.txt cnv_data.txt -I input/ -O ./

```

#### Build phyloWGS Docker
```
sudo usermod -a -G docker ${USER}
# clone this repo
git clone https://merckey@bitbucket.org/merckey/google_cloud.git
cd google_cloud
# then run docker build
sudo docker build -t phylowgs/test ./phylowgs/
```

#### Push Docker image to google container registry

[container registry](https://cloud.google.com/container-registry/docs/pushing-and-pulling)

**replace** the `nih-commons-credit-project` with your **project-id**

Follow this guide [How to get your project id](https://cloud.google.com/resource-manager/docs/creating-managing-projects#identifying_projects)
to find your project id.

```
# tag the image
sudo docker tag phylowgs/test us.gcr.io/nih-commons-credit-project/phylowgs:test
# push it to the container registry
sudo gcloud docker -- push us.gcr.io/nih-commons-credit-project/phylowgs:test
```

#### Install dsub

dsub require the Genomics API to work [Genomics API](https://console.cloud.google.com/apis/dashboard?project=nih-commons-credit-project&authuser=1&duration=PT1H)
```
gcloud auth application-default login
Credentials saved to file: [/home/lji226/.config/gcloud/application_default_credentials.json]
dsub \
  --project nih-commons-credit-project \
  --zones "us-central1-*" \
  --logging gs://bucket1q2w/jpliu/logging/ \
  --output OUT=gs://bucket1q2w/jpliu/out.txt \
  --image us.gcr.io/nih-commons-credit-project/phylowgs:test \
  --command 'echo "Hello World" > "${OUT}"' \
  --wait
```

#### Debug run
```
dsub   --min-cores 4   --min-ram 10   --disk-size 100  \
--project nih-commons-credit-project   --zones "us-east1-*"   \
--logging gs://bucket1q2w/austin/titancna/tcga_luad/logging   \
--image us.gcr.io/nih-commons-credit-project/titancna_luad:latest  \
--input-recursive liftovers=gs://bucket1q2w/austin/titancna/tcga_luad/liftovers   \
--input-recursive refs=gs://bucket1q2w/jpliu/titancna/tcga/refgrch38/   \
--input bamMeta=gs://bucket1q2w/austin/titancna/tcga_luad/luad_pass_bam.tsv   \
--input samplesList=gs://bucket1q2w/austin/titancna/tcga_luad/TCGA_LUAD_filtered_samples.yaml   \
--input pairList=gs://bucket1q2w/austin/titancna/tcga_luad/TCGA_LUAD_filtered_pairings.yaml   \
--input token=gs://bucket1q2w/austin/titancna/tcga_luad/gdc-user-token.2018-12-07T19_22_07.618Z.txt   \
--output-recursive OUTPUT_DIR=gs://bucket1q2w/austin/titancna/tcga_luad/luad   --tasks run1.tsv  \
--command "cd /home/usr/titancna/scripts/snakemake/ && sleep 200000"   --wait
```