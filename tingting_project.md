## Tingting's project

[READ dsub manual](https://github.com/googlegenomics/dsub)

#### Build docker

  1. Make sure you are under your home directory. Clone this repo.

    ```
    cd ~/ && git clone https://merckey@bitbucket.org/merckey/google_cloud.git
    ```

  1. Upload your entire code to the directory named `code`. It must be located at home directory `~/`. The direcotry must be named as `code`. 
    ```
    ls code/
    ```

  1. Build and tag. This will only work when your current path is your home directory

    ```
    sudo docker build . -f google_cloud/tingting/Dockerfile -t tingting:latest  
    ```
    
    ```
    sudo docker tag tingting:latest us.gcr.io/nih-commons-credit-project/tingting:latest
    ```

  1. Push to container registry

    ```
    sudo gcloud docker -- push us.gcr.io/nih-commons-credit-project/tingting:latest
    ```

**Note:**

Everytime you changed your code in the `code` directory, you must repeat steup 3 and 4 otherwise, the docker image will not be updated when you submit
dsub jobs.

#### Debug on local instance with dsub
Before you do this make sure you have the input file in your google cloud bucket, in my case `gs://bucket1q2w/jpliu/debug/tingting/data`.

  1. `A6.2671.txt`

The command below will not submit job by creating new instance, but run on your current working instance.

After the run, you will find the results under your google bucket `gs://bucket1q2w/jpliu/debug/tingting/results/`


```
dsub \
  --provider local \
  --logging gs://bucket1q2w/jpliu/debug/tingting/logging \ 
  --image tingting:latest \
  --input-recursive GB=gs://bucket1q2w/jpliu/debug/tingting/data/ \
  --output-recursive OUTPUT=gs://bucket1q2w/jpliu/debug/tingting/results \
  --tasks google_cloud/tingting/example.txt \
  --command "bash /tmp/dev/run_code.sh" \
  --wait
```
Explanation:
  
  1. --input-recursive GB: the google bucket input folder. You must store all you input data file in this folder before submitting jobs.
  1. --output-recursive OUTPUT: the google bucket output folder you want to save the results.
  1. --tasks: a text file used for submitting batch jobs. You can find more information using this [link](https://github.com/DataBiosphere/dsub#submitting-a-batch-job). Basically, it's 
  a tab delimited file. The `google_cloud/tingting/example.txt` format will work for this project. To run you desired jobs, add them to the list including the parameters. **For debugging
  purpose, select small samples and parameters to save time**.

**IMPORTANT**: Everytime you run dsub command, samples included in the example.txt will be submitted to run. Make sure you don't run the finished sample repeatedly.


When you've successfully run the code above and get the output file in `gs://bucket1q2w/jpliu/debug/tingting/results/`, you can then
try to execute the code below.

#### Run by creating new instances for batch jobs
This will submit all the samples included in the `example.txt` to GCP. The default instance provides CPU = 1, RAM = 3.75GB. If you need to use larger instance, you can set `--min-ram` and `--min-cores` parameters.
```
dsub \
  --project nih-commons-credit-project \
  --provider google-v2 \
  --zones "us-east1-*" \
  --logging gs://bucket1q2w/jpliu/debug/tingting/logging \
  --image us.gcr.io/nih-commons-credit-project/tingting:latest \
  --input-recursive GB=gs://bucket1q2w/jpliu/debug/tingting/data/ \
  --output-recursive OUTPUT=gs://bucket1q2w/jpliu/debug/tingting/results \
  --tasks google_cloud/tingting/example.txt \
  --command "bash /tmp/dev/run_code.sh" \
  --wait
```

## CLOSE IDLE INSTANCE